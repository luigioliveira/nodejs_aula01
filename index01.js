const{createServer} = require('http');

let server = createServer((request, response) => {
    response.writeHead(200,{"Content-type": "text/html"});
    response.write('<h1>Hello world!</h1>');
    response.end();
});

server.listen(8000)