const express = require('express');
const port = 8000;
let app = express();

app.get("/", (req, res) => {
    res.send("Resultado sucesso raíz");
    console.log("GET /");
});

app.get("/clientes", (req, res)=>{
    res.send("Resultado sucesso para o acesso aos clientes")
    console.log("GET /clientes")
})

app.get("/financeiro", (req, res)=>{
    let object = req.query
    res.json({
        "message": "Desenvolvido durante a aula de Técnicas de Programação de 22 de março de 2022 do Professor Gobbato.",
        "name": object.name, 
        "valor": object.valor
    })
    console.log("GET /financeiro")
})

app.listen(port, () => {
    console.log(`Projeto executando na porta ${port}`);
})